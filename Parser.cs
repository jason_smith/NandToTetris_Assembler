﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assembler
{
    static class Parser
    {
        public static ISymbolicInstruction Parse(string line)
        {
            string instStr = RemoveTrailingComments(line);

            if (instStr[0] == '(')
                return ParseLabel(instStr);

            if (instStr[0] == '@')
                return ParseAInstruction(instStr);

            return ParseCInstruction(instStr);
        }

        // (LABEL)
        private static Label ParseLabel(string line)
        {
            int index = line.IndexOf(')');
            if (index < 0)
                throw new ArgumentException("No matching parenthesis found: " + line);

            string labelStr = line.Substring(1, index - 1);
            var label = new Label(labelStr);
            return label;
        }

        // @address
        private static AInstruction ParseAInstruction(string line)
        {
            string valueStr = line.Substring(1);

            int address;
            bool isAddress = int.TryParse(valueStr, out address);
            var aInst = isAddress ? new AInstruction(address) : new AInstruction(valueStr);
            return aInst;
        }

        // dest=comp;jmp
        private static CInstruction ParseCInstruction(string line)
        {
            string instStr = line;

            string dest = null;

            string[] parts = instStr.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length == 2)
            {
                dest = parts[0].Trim();
                instStr = parts[1];
            }
            else if (parts.Length > 2)
                throw new InvalidOperationException("Multiple assignments detected: " + line);

            string comp = null;
            string jmp = null;

            parts = instStr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            comp = parts[0].Trim();
            if (parts.Length == 2)
                jmp = parts[1].Trim();
            else if (parts.Length > 2)
                throw new InvalidOperationException("Multiple semicolons detected: " + line);

            var cInst = new CInstruction(comp, dest, jmp);
            return cInst;
        }

        private static string RemoveTrailingComments(string instStr)
        {
            int index = instStr.IndexOf("//");
            return (index > -1) ? instStr.Substring(0, index).TrimEnd() : instStr;
        }
    }
}
