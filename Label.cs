﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assembler
{
    [DebuggerDisplay("Value={_value}")]
    class Label : ISymbolicInstruction
    {
        readonly string _value;

        public Label(string value)
        {
            _value = value;
        }

        public string Value
        {
            get { return _value; }
        }
    }
}
