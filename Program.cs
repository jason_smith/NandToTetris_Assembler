﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assembler
{
    class Program
    {
        const string AsmExt = ".asm";
        const string HackExt = ".hack";
        const string Comment = "//";

        private static void Assemble(string sourceFile)
        {
            Console.WriteLine();
            Console.WriteLine("Starting assembly process on " + sourceFile + "...");

            string[] lines = File.ReadAllLines(sourceFile);

            Console.WriteLine("Parsing instructions and generating symbol table...");
            var symbolTable = new SymbolTable();
            List<ISymbolicInstruction> instructions;
            bool success = ParseSource(lines, symbolTable, out instructions);
            if (!success)
            {
                Console.WriteLine("Failed to parse source file: " + sourceFile);
                return;
            }

            string destPath = Path.ChangeExtension(sourceFile, HackExt);
            Console.WriteLine("Generating '" + Path.GetFileName(destPath) + "' file...");
            success = Translate(instructions, symbolTable, destPath);
            if (success)
                Console.WriteLine("Assembly completed successfully: " + destPath);
            else
                Console.WriteLine("Assembly failed.");
        }

        static void Main(string[] args)
        {
            if (!ValidateArgs(args))
                return;

            foreach (string arg in args)
                Assemble(arg);
        }

        private static bool ParseSource(string[] lines, SymbolTable symbolTable, out List<ISymbolicInstruction> instructions)
        {
            instructions = new List<ISymbolicInstruction>();
            int instCount = 0;
            int lineNum = 0;

            try
            {
                for (; lineNum<lines.Length; lineNum++)
                {
                    string instStr = lines[lineNum].Trim();
                    if (instStr.StartsWith(Comment) || string.IsNullOrEmpty(instStr))
                        continue;

                    ISymbolicInstruction inst = Parser.Parse(instStr);
                    var label = inst as Label;
                    if (label != null)
                        symbolTable.Add(label.Value, instCount);
                    else
                    {
                        instructions.Add(inst);
                        instCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error encountered on line " + lineNum.ToString() + ": " + ex.Message);
                return false;
            }

            return true;
        }

        private static bool Translate(List<ISymbolicInstruction> instructions, SymbolTable symbolTable, string destPath)
        {
            StreamWriter writer = null;

            try
            {
                if (File.Exists(destPath))
                    File.Delete(destPath);

                writer = File.CreateText(destPath);

                foreach (ISymbolicInstruction inst in instructions)
                {
                    UpdateInstruction(inst, symbolTable);

                    string binaryStr = Encoder.Encode(inst);
                    writer.WriteLine(binaryStr);
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error encountered: " + ex.Message);
                return false;
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        private static void UpdateInstruction(ISymbolicInstruction instruction, SymbolTable symbolTable)
        {
            var aInst = instruction as AInstruction;
            if (aInst != null && aInst.IsSymbolic)
            {
                int address;
                bool addressFound = symbolTable.TryGetAddress(aInst.Symbol, out address);
                if (!addressFound)
                {
                    // symbol not in table, thus it is a variable that needs added to the symbol table
                    address = symbolTable.AddVariable(aInst.Symbol);
                }

                aInst.Address = address;
            }
        }

        private static bool ValidateArgs(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("One or more valid *.asm files must be specified.");
                return false;
            }

            bool invalid = false;
            foreach (string arg in args)
            {
                if (!File.Exists(arg))
                {
                    Console.WriteLine("File not found: " + arg);
                    invalid = true;
                }

                string ext = Path.GetExtension(arg);
                if (!string.Equals(ext, AsmExt, StringComparison.OrdinalIgnoreCase))
                {
                    Console.WriteLine("Invalid file extension: '" + ext + "'. Valid file type is '" + AsmExt + "'.");
                    invalid = true;
                }
            }

            if (invalid)
                return false;

            return true;
        }
    }
}
