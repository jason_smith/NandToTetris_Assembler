﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assembler
{
    class SymbolTable
    {
        readonly Dictionary<string, int> _st;
        int _nextVarAddress;

        public SymbolTable()
        {
            _nextVarAddress = 16;
            _st = new Dictionary<string, int>()
            {
                { "R0", 0 },
                { "R1", 1 },
                { "R2", 2 },
                { "R3", 3 },
                { "R4", 4 },
                { "R5", 5 },
                { "R6", 6 },
                { "R7", 7 },
                { "R8", 8 },
                { "R9", 9 },
                { "R10", 10 },
                { "R11", 11 },
                { "R12", 12 },
                { "R13", 13 },
                { "R14", 14 },
                { "R15", 15 },
                { "SP", 0 },
                { "LCL", 1 },
                { "ARG", 2 },
                { "THIS", 3 },
                { "THAT", 4 },
                { "SCREEN", 16384 },
                { "KBD", 24576 }
            };
        }


        public void Add(string symbol, int address)
        {
            _st.Add(symbol, address);
        }

        public int AddVariable(string symbol)
        {
            int address = _nextVarAddress;
            _st.Add(symbol, address);
            _nextVarAddress++;

            return address;
        }

        public bool ContainsSymbol(string symbol)
        {
            return _st.ContainsKey(symbol);
        }

        public void Remove(string symbol)
        {
            _st.Remove(symbol);
        }

        public bool TryGetAddress(string symbol, out int address)
        {
            return _st.TryGetValue(symbol, out address);
        }
    }
}
