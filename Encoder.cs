﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assembler
{
    static class Encoder
    {
        public static string Encode(ISymbolicInstruction instruction)
        {
            var aInst = instruction as AInstruction;
            if (aInst != null)
                return EncodeAInstruction(aInst);

            var cInst = instruction as CInstruction;
            if (cInst != null)
                return EncodeCInstruction(cInst);

            throw new ArgumentException("Invalid instruction type");
        }

        private static string EncodeAInstruction(AInstruction instruction)
        {
            string addrStr = Convert.ToString(instruction.Address, 2);
            if (addrStr.Length > 15)
                throw new InvalidOperationException("Address string contains too many bits: " + addrStr);

            string zeroPadding = string.Empty;

            int zeroPadCount = 16 - addrStr.Length;
            if (zeroPadCount > 5)
            {
                var sb = new StringBuilder();
                for (int i = 0; i < zeroPadCount; i++)
                    sb.Append("0");

                zeroPadding = sb.ToString();
            }
            else
            {
                for (int i = 0; i < zeroPadCount; i++)
                    zeroPadding += "0";
            }

            string binInst = zeroPadding + addrStr;
            return binInst;
        }

        private static string EncodeCInstruction(CInstruction instruction)
        {
            string binComp = GetCompCode(instruction.Comp);
            string binDest = GetDestCode(instruction.Dest);
            string binJump = GetJumpCode(instruction.Jmp);

            string binInstr = "111" + binComp + binDest + binJump;
            return binInstr;
        }

        private static string GetCompCode(string symComp)
        {
            string cStr = null;

            switch (symComp)
            {
                case "0":
                    cStr = "101010";
                    break;
                case "1":
                    cStr = "111111";
                    break;
                case "-1":
                    cStr = "111010";
                    break;
                case "D":
                    cStr = "001100";
                    break;
                case "A":
                case "M":
                    cStr = "110000";
                    break;
                case "!D":
                    cStr = "001101";
                    break;
                case "!A":
                case "!M":
                    cStr = "110001";
                    break;
                case "-D":
                    cStr = "001111";
                    break;
                case "-A":
                case "-M":
                    cStr = "110011";
                    break;
                case "D+1":
                case "1+D":
                    cStr = "011111";
                    break;
                case "A+1":
                case "1+A":
                case "M+1":
                case "1+M":
                    cStr = "110111";
                    break;
                case "D-1":
                case "-1+D":
                    cStr = "001110";
                    break;
                case "A-1":
                case "-1+A":
                case "M-1":
                case "-1+M":
                    cStr = "110010";
                    break;
                case "D+A":
                case "A+D":
                case "D+M":
                case "M+D":
                    cStr = "000010";
                    break;
                case "D-A":
                case "-A+D":
                case "D-M":
                case "-M+D":
                    cStr = "010011";
                    break;
                case "A-D":
                case "-D+A":
                case "M-D":
                case "-D+M":
                    cStr = "000111";
                    break;
                case "D&A":
                case "A&D":
                case "D&M":
                case "M&D":
                    cStr = "000000";
                    break;
                case "D|A":
                case "A|D":
                case "D|M":
                case "M|D":
                    cStr = "010101";
                    break;
            }

            if (cStr == null)
                throw new InvalidOperationException("Invalid symbolic comp detected: " + symComp);

            string aStr = symComp.Contains('M') ? "1" : "0";
            return aStr + cStr;
        }

        private static string GetDestCode(string symDest)
        {
            if (string.IsNullOrEmpty(symDest))
                return "000";

            switch (symDest)
            {
                case "M": 
                    return "001";
                case "D": 
                    return "010";
                case "MD": 
                case "DM": 
                    return "011";
                case "A": 
                    return "100";
                case "AM": 
                case "MA": 
                    return "101";
                case "AD": 
                case "DA": 
                    return "110";
                case "AMD": 
                case "ADM": 
                case "MAD": 
                case "MDA": 
                case "DMA": 
                case "DAM": 
                    return "111";
            }

            throw new InvalidOperationException("Invalid symbolic dest detected: " + symDest);
        }

        private static string GetJumpCode(string symJmp)
        {
            if (string.IsNullOrEmpty(symJmp))
                return "000";

            switch (symJmp)
            {
                case "JGT": return "001";
                case "JEQ": return "010";
                case "JGE": return "011";
                case "JLT": return "100";
                case "JNE": return "101";
                case "JLE": return "110";
                case "JMP": return "111";
            }

            throw new InvalidOperationException("Invalid symbolic jump detected: " + symJmp);
        }
    }
}
