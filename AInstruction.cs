﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assembler
{
    [DebuggerDisplay("Symbol={_symbol}, Address={Address}")]
    class AInstruction : ISymbolicInstruction
    {
        readonly string _symbol;

        public AInstruction(string symbol)
        {
            _symbol = symbol;
        }

        public AInstruction(int address)
        {
            this.Address = address;
        }

        public int Address { get; set; }

        public bool IsSymbolic
        {
            get { return !string.IsNullOrEmpty(_symbol); }
        }

        public string Symbol
        {
            get { return _symbol; }
        }
    }
}
