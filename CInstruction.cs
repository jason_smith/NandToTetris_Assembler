﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assembler
{
    [DebuggerDisplay("Dest={_dest}, Comp={_comp}, Jmp={_jmp}")]
    class CInstruction : ISymbolicInstruction
    {
        readonly string _comp;
        readonly string _dest;
        readonly string _jmp;

        public CInstruction(string comp, string dest, string jmp)
        {
            _comp = comp;
            _dest = dest;
            _jmp = jmp;
        }

        public string Comp
        {
            get { return _comp; }
        }

        public string Dest
        {
            get { return _dest; }
        }

        public string Jmp
        {
            get { return _jmp; }
        }
    }
}
